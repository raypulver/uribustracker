# URIBusTracker

The purpose of this software is to provide a smart display of the locations of the buses at URI, using only the inaccurate and sparsely updated GPS coordinates offered by RIPTA as inputs to the program. Every time updated coordinates are received from RIPTA, the buses animate along a path, displaying their new most recent location.

This program makes two key assumptions:

  * The buses either operate on the "Hill Climber" route or "Blue" route, and both these routes follow one deterministic path in a loop.
  * The real GPS coordinates of the buses are the closest point between the line interpolating the roads between the bus stops and the received GPS coordinates.

The application checks for updated bus coordinates at a configurable interval in a background thread, and notifies the MapActivity instance whenever updated data is received, which is then parsed from its JSON representation and into a BusData object. The observing object, the instance of MapActivity, maintains a map of Bus objects which contain the last known point and the new updated coordinates, and it is using this information that the coordinates of each point in an interpolated path can be calculated. The interpolation function is private to Route and is the square of a cosine function, providing a sharper increase and decrease at the beginning and end of the animation than the one provided by android.view.animation.AccelerateDecelerateInterpolator, which uses a first order cosine function. The resulting animation can be played over a variable amount of frames, and is the major feature of URIBusTracker.

URIBusTracker makes use of multithreading. All containers accessed from background threads are synchronized among threads so there is no data race condition.

## Install 

Building the APK file requires the Android SDK in the PATH and a working Java installation.

The source code for this repository can be retrieved with

```
git clone https://bitbucket.org/raypulver/uribustracker
cd uribustracker
git checkout submission
git fetch
```

If no git installation is present, the archive can be retrieved from [here](https://bitbucket.org/raypulver/uribustracker/get/submission.zip)

To build, in the project directory, run

```
./gradlew build
```

The application will be available in *app/build/outputs/apk*

To run the tests, run

```
./gradlew test
```

Alternatively, one can simply download Android Studio from the website and open the project folder in the program, where the application can be run on the host machine via emulator. In order to do this you need to use the file debug.keystore in the project root directory, replacing the old debug.keystore on the host machine. On Mac and UNIX this will be in the *~/.android* folder.
