package edu.uri.uribustracker;

import org.junit.Test;

import static org.junit.Assert.*;
import com.google.android.gms.maps.model.LatLng;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by ray on 4/25/16.
 */
public class UtilTest {
    @Test
    public void testSetCopy() throws Exception {
        HashSet<String> hs = new HashSet<String>();
        hs.add("test1");
        hs.add("test2");
        HashSet<String> copy = Util.setCopy(hs);
        assertEquals(copy.size(), 2);
        Iterator<String> it = copy.iterator();
        String item = it.next();
        assertEquals(item.equals("test1") || item.equals("test2"), true);
        item = it.next();
        assertEquals(item.equals("test1") || item.equals("test2"), true);
    }
}
