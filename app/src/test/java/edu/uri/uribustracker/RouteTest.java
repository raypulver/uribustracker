package edu.uri.uribustracker;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ray on 4/25/16.
 */
public class RouteTest {
    static Route route1;
    static Route route2;
    @BeforeClass
    public static void setUpClass() {
        double coords[] = {0, 0, 0.5, 0.5};
        double othercoords[] = {1, 1, 2, 2};
        route1 = new Route(coords, 1, 16.0f);
        route2 = new Route(othercoords, 1, 16.0f);
    }
    @Test
    public void testClosestPoint() throws Exception {
        LatLng l = new LatLng(1, 1);
        Route.InterpolationResult result = route1.closestPoint(l);
        assertEquals(Math.abs(result.point.latitude - 0.5) < 1e-5, true);
        assertEquals(Math.abs(result.point.longitude - 0.5) < 1e-5, true);
    }
    @Test
    public void testMinDistance() throws Exception {
        LatLng spot = new LatLng(1, 1);
        assertEquals(Math.abs(route1.minDistance(route1.coords.get(0), route1.coords.get(1), spot) - 1) < 1e-5, true);
        spot = new LatLng(0.25, 0.75);
        assertEquals(Math.abs(route1.minDistance(route1.coords.get(0), route1.coords.get(1), spot) - 0.5) < 1e-5, true);
    }
    @Test
    public void testInterpolation() throws Exception {
        // We are testing that the boundaries are correct, rather than the actual metrics of the function
        assertEquals(Math.abs(Route.accelerateDecelerateInterpolator(1.0) - 1.0) < 1e-5, true);
        assertEquals(Math.abs(Route.accelerateDecelerateInterpolator(0)) < 1e-5, true);
    }
    @Test
    public void testDistance() throws Exception {
        LatLng spot = new LatLng(1, 1);
        assertEquals(Math.abs(route1.distance(route1.coords.get(0), route1.coords.get(1), spot, 0.5) - Math.sqrt(1.0/2)) < 1e-5, true);
    }
    @Test
    public void testLinearInterpolator() throws Exception {
        assertEquals(Math.abs(route1.getInterpolatedLongitude(route1.coords.get(0), route2.coords.get(1), 0.25) - 0.25) < 1e-5, true);
    }
}
