package edu.uri.uribustracker;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import java.util.Observable;
import com.google.android.gms.maps.model.LatLng;
import java.util.Timer;
import java.util.TimerTask;
import android.content.pm.PackageManager;
/**
 * Created by ray on 4/29/16.
 *
 * This class gathers information about the GPS location of the busses.
 * We get this data form the Google Android Maps feature.
 * This class also allows the map to update properly, placing the buses in
 * their correct positions.
 *
 * @author emilyhendricks
 */
public class GPSLocationFetcher extends Observable implements LocationListener {

    public void onStatusChanged(String s, int i, Bundle b) {
    }

    /**
     * A timer is instantiated as well as a Location manager
     */
    private Timer mTimer;
    private LocationManager mLocationManager;

    /**
     * GPSLocationFetcher recieves a LocationManager, creates a new
     * timer and schedules a poll so that requests for coordinates can be made.
     * @param lm
     */
    public GPSLocationFetcher(LocationManager lm) {
        mLocationManager = lm;
        mTimer = new Timer();
        schedulePoll();
    }

    /**
     * The schedulePoll function will request the
     * gps coordinates after a given amount of time has passed.
     */
    public void schedulePoll() {
        mTimer.schedule(new TimerTask() {
            public void run() {
                poll();
                schedulePoll();
            }
        }, 5000);
    }

    /**
     * The poll function requests the gps coordinates of the bus.
     */
    public void poll() {
        if (ContextCompat.checkSelfPermission(MapActivity.context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
        }
    }
    public void onProviderDisabled(String s) {}
    public void onProviderEnabled(String s) {}

    /**
     * The onLocation function obtains a latitude and longitude then
     * changes the desired location to those coordinates clearing
     * the old data in the process.
     *
     */
    public void onLocationChanged(Location l) {
        LatLng loc = new LatLng(l.getLatitude(), l.getLongitude());
        setChanged();
        notifyObservers(loc);
        clearChanged();
    }
}
