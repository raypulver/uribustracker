package edu.uri.uribustracker;

/**
 * Created by ray on 4/17/16.
 *
 * This class allows the user to select which route they to appear on the map.
 * It gives the user the ability to toggle between making the Hill Climber
 * and the Blue Line routes, along with the shuttle busses on those routes visible.
 * This feature has not been completed.
 *
 *  @author emilyhendricks
 */
public class RouteSelector{
    /**
     * MapActivity being where most of the functionality occurs, is called upon.
     *
     */
    public static MapActivity mMapActivity;
    public static void setMapActivity(MapActivity a) {
        mMapActivity = a;
    }

    /**
     * The two different routes are given an enum value. The selected routes are ORed into the selection variable.
     */
    static int selection = 0;
    static int RED_ROUTE = 0x01;
    static int BLUE_ROUTE = 0x02;

    /**
     * Redraw the associated MapActivity if it exists
     * @return boolean representing if a redraw action was taken.
     */
    private static boolean redraw() {
        if (mMapActivity != null) {
            mMapActivity.redraw();
            return true;
        }
        return false;
    }

    /** unselects all routes
     * @return boolean representing if there was a change
     */
    public static boolean unselect() {
        boolean hadChange = selection != 0;
        selection = 0;
        redraw();
        return hadChange;
    }

    /** unselects route based on enum value
     *
     * @param m enum value of the route
     * @return boolean representing if there was a change.
     */
    public static boolean unselect(int m) {
        boolean hadChange = (selection & m) != 0;
        selection &= ~m;
        redraw();
        return hadChange;
    }

    /** Toggles if a route is selected based on enum value
     *
     * @param m The enum value of the route
     * @return A boolean representing if the map activity was updated as a result of the operation.
     */
    public static boolean toggle(int m) {
        selection ^= m;
        return redraw();
    }

    /** Toggles a route based on its static reference value
     *
     * @param r Reference to the route
     * @return A boolean representing whether or not the map activity was updated as a result of the operation.
     */
    public static boolean toggle(Route r) {
        selection ^= routeToEnum(r);
        return redraw();
    }

    /**
     * The select function enables a route to be selected
     * @param m
     * @return A boolean representing if the operation changed the selection value.
     */
    public static boolean select(int m) {
        boolean hadChange = (selection & m) == 0;
        selection |= m;
        redraw();
        return hadChange;
    }

    /**
     * The routeToEnum function returns the enum value for the route.
     * @param r Route reference
     * @return The enum value of the route
     */
    private static int routeToEnum(Route r) {
        if (r == Route.getBlueRoute()) {
            return BLUE_ROUTE;
        } else if (r == Route.getHillClimberRoute()) {
            return RED_ROUTE;
        }
        return 0;
    }
    /**
     * The isSelected function checks to see if the given route is selected.
     * @param r Route reference
     * @return boolean representing if the route is currently selected.
     */
    static boolean isSelected(Route r) {
        if ((routeToEnum(r) & selection) != 0) {
            return true;
        }
        return false;
    }
}
